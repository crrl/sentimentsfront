import React from 'react';
import './App.css';
import Main from './components/General/Router/Router';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div>
      <Main />
    </div>
  );
}

export default App;
