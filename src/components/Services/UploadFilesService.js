import Axios from '../../axiosConfig';

class UploadFilesService {
  upload(files) {
    let formData = new FormData();

    for(let x = 0; x<files.length; x++) {
      formData.append('file', files[x]);
  }
    return Axios.post("/api/upload", formData);
  }

  getFiles() {
    return Axios.get("/api/files");
  }

  readFile(fileName) {
    let payload = {
      fileName,
    }
    return Axios.post("/api/file", payload);
  }
}

export default new UploadFilesService();