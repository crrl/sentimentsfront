import React from 'react';
import { Switch } from 'react-router-dom'

import { Login, UploadFiles, NewLogin } from '../../../views/index';

import PrivateRoute from '../PrivateRoute/PrivateRoute';
import ValidateLoginRoute from '../PrivateRoute/ValidateLoginRoute';

const Main = () => (
    <>
      <Switch>
        <PrivateRoute exact path='/' component={UploadFiles} />
        <ValidateLoginRoute exact path='/login' component={Login}/>
        <ValidateLoginRoute exact path='/nuevo-usuario' component={NewLogin}/>
      </Switch>
    </>
  )

  export default Main;