import React, { Component } from 'react';
import { Col, Toast } from 'react-bootstrap';
import './Toast.css';

export default class CustomToast extends Component {

    constructor(props) {
      super(props);
      this.state = {
        showToast: this.props.show
      }
    }

  render() {
    return (
      <Col className="fixed">
        <Toast onClose={() => this.setState({showToast:false})} show={this.state.showToast} delay={3000} autohide className="toast-background right">
          <Toast.Header>
            <strong className="mr-auto">Test</strong>
          </Toast.Header>
          <Toast.Body>{this.props.message}</Toast.Body>
        </Toast>
      </Col>
    )
  }
}