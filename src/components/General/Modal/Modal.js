import React from "react";
import Modal from "react-bootstrap/Modal";
import './Modal.css'
import UploadService from "../../Services/UploadFilesService";


export default function Example(props) {
  const [isOpen, setIsOpen] = React.useState(false);
  const [title, setTitle] = React.useState("");
  const [fileContent, setFileContent] = React.useState("")
  const showModal = () => {
    setIsOpen(true);
    readFile(props.fullName)
    setTitle(props.fileName)
  };

  const hideModal = () => {
    setIsOpen(false);
  };

  const readFile = (fileName) => {
    UploadService.readFile(fileName).then((response) => {
      setFileContent(response.data)
    });
  }

  return (
    <>
      <button onClick={showModal}>{props.fileName}</button>
      <Modal
        show={isOpen}
        onHide={hideModal}
        size="xl"
      >
        <Modal.Header className={'center-flex'}>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body><p className={'text-properties'}>{fileContent}</p></Modal.Body>
        <Modal.Footer>
          <button onClick={hideModal}>Cerrar</button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
