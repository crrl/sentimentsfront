import React, { Component } from 'react';
import { Navbar, Nav, Form, Button } from 'react-bootstrap';

import './Header.css'

export default class Header extends Component {
  state = {
    redirect: false
  }
  
  changeScreen = (location) => {
    this.setState({
      redirect: true,
      redirectTo: location
    });
  }

  logOut = () => {
    sessionStorage.clear();
    window.location.reload();
  };

  render() {
    return(
      <Navbar bg="dark" variant="dark" className="custom-properties">
        <Navbar.Brand onClick={() => this.changeScreen('/')}>Examen sentimientos</Navbar.Brand>
        <Nav className="mr-auto">
        </Nav>
        <Form inline>
          <Button onClick={() => this.logOut()}>Salir</Button>
        </Form>
      </Navbar>
    )
  }
}