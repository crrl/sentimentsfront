import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Header from '../../General/Header/Header';


function PrivateRoute({ component: Component, ...rest }) {
  let isTokenValid = !!sessionStorage.getItem('token');
  return(
    <Route {...rest} render={(props) => (
      isTokenValid ?
      <span><Header /><Component {...props} /> </span>:
      <Redirect to="/login" />
    )}
    />
  );
}

export default PrivateRoute;