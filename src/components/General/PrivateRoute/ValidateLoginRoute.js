import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function ValidateLoginRoute({ component: Component, ...rest }) {
  let isTokenValid = !sessionStorage.getItem('token');
  return(
    <Route {...rest} render={(props) => (
      isTokenValid ?
      <Component {...props} /> :
      <Redirect to="/" />
    )}
    />
  );
}

export default ValidateLoginRoute;