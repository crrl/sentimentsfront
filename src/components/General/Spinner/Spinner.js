import React, { Component } from 'react';


const Spinner = () => {
  return (
    <div className="lds-hourglass hide-component"></div>
  );
}

export default Spinner;