import axios from 'axios';


const instance = axios.create({
  baseURL: 'http://localhost:30014'
});

instance.interceptors.request.use(config => {
  document.getElementsByClassName('lds-hourglass')[0].classList.remove('hide-component');
  
  



  if (config.url !== '/api/authentication') {
    if (!instance.defaults.headers.common['Authorization']) {
      instance.defaults.headers.common['Authorization'] = sessionStorage.token;
      config.headers.common.Authorization = sessionStorage.token;
    }
    let currentDate = new Date().getTime() / 1000;
    if (parseFloat(currentDate) > parseFloat(sessionStorage.getItem('exp'))) {
      sessionStorage.clear();
      window.location.reload();
    }
  }
  return config;
});

instance.interceptors.response.use(config => {
  document.getElementsByClassName('lds-hourglass')[0].classList.add('hide-component');
  return config;
}, (error) => {
  if ([401].indexOf(error && error.response && error.response.status) >= 0) {
    sessionStorage.clear();
    window.location.reload();
    document.getElementsByClassName('lds-hourglass')[0].classList.add('hide-component');
  } else {
    document.getElementsByClassName('lds-hourglass')[0].classList.add('hide-component');
    return Promise.reject(error);
  }
})

export default instance;
