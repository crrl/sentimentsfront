import Login from './Login/login';
import UploadFiles from './UploadFiles/UploadFiles';
import NewLogin from './NewLogin/NewLogin';

export {
  Login,
  UploadFiles,
  NewLogin
}