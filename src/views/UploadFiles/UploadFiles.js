import React, { Component } from "react";
import UploadService from "../../components/Services/UploadFilesService";

import './UploadFiles.css';
import Modal from '../../components/General/Modal/Modal';
export default class UploadFiles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFiles: undefined,
      message: null,

      fileInfos: [],
      sentiments:[],
    };
  }

  selectFile = (event) => {
    this.setState({
      selectedFiles: event.target.files,
    });
  }

  uploadFiles = () => {
    const selectedFiles = this.state.selectedFiles;

    this.setState(
      {
        message: null,
      }
    );

    UploadService.upload(selectedFiles)
      .then((files) => {
          window.location.reload();
      })
      .catch(() => {
        this.setState({
          message: "Archivo no permitido.",
        });
      });    
  }
    

  componentDidMount() {
    UploadService.getFiles().then((response) => {
      this.setState({
        fileInfos: response.data.data,
        sentiments: response.data.sentiments
      });
    });
  }


  render() {
    const { selectedFiles, message, fileInfos, sentiments } = this.state;

    return (
      <>
         <div className={'files-content'}>
          <label className="btn btn-default ">
            <input type="file" className={"white"} multiple onChange={this.selectFile} />
          </label>

          <button
            className="btn btn-success"
            disabled={!selectedFiles}
            onClick={this.uploadFiles}
          >
            Subir archivo(s)
          </button>

          {message && (
            <div className="alert alert-light" role="alert">
              <ul>
                {message.split("\n").map((item, i) => {
                  return <li key={i}>{item}</li>;
                })}
              </ul>
            </div>
          )}

          <div className="card">
            <div className="card-header">Listado de Comentarios</div>
            <ul className="list-group list-group-flush">
              {fileInfos &&
                fileInfos.map((file, index) => (
                  <li className="list-group-item" key={index}>
                    <Modal fullName={file.fullName} fileName={file.fileName}></Modal>
                  </li>
                ))}
            </ul>
          </div>
        </div>
        <div className="card">
          <div className="card-header">Listado de Sentimientos</div>

          <div className={"sentiment-content"}>
            <ul className="list-group list-group-flush">
            {sentiments &&
                    sentiments.map((sentiment, index) => (
                      <li className="list-group-item" key={index}>
                      <p>Fecha: <span className="bold">{sentiment.date.substring(0,10)} </span></p>
                      <p>Comentario: <span className="bold">{sentiment.fileName}</span></p>
                      <p>Tipo de sentimiento: <span className="bold">{sentiment.type}</span></p>
                      <p>Polaridad: <span className="bold">{sentiment.polarity}</span></p>
                      </li>
                    ))}
              </ul>
          </div>
        </div>
      </>
    );
  }
}