import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import CustomToast from '../../components/General/Toast/Toast';
import Axios from '../../axiosConfig';

export default class Login extends Component {

  state = {
    name:'',
    email:'',
    password:'',
    showToast: false,
    key:0,
    redirect: false
  }

  save = () => {
    const {name, email, password} = this.state;
    let key = this.state.key+1;
    if(!name || !email || !password || !/@coppel.com\s*$/.test(email)) {
      this.setState({
        showToast: true,
        key: key
      });
    } else {
      let payload = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password
      };
  
      Axios.post('/api/user',payload)
      .then(res => {
        this.setState({
          redirect: true
        });
      })
      .catch(err => {
        this.setState({
          showToast: true,
          key: key
        });
      });
    }
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to='/login'/>;
    }

    return (  
      <div>
        {this.state.showToast ? <CustomToast show={true} key={this.state.key} message="Algo ha salido mal, intenta de nuevo!"/>: <span></span>}
        <Row className="justify-content-md-center absolute-center center" >
          <Col lg="3">
            <div className="form-group left-align white">
            <label htmlFor="nombre">Nombre:</label>
              <input type="text" className="form-control" id="nombre" onChange={(ev) => {this.setState({name: ev.target.value})}} />
            <label htmlFor="email">Correo:</label>
              <input type="email" className="form-control" id="email" onChange={(ev) => {this.setState({email: ev.target.value})}} />
              <label htmlFor="password">Contraseña:</label>
              <input type="password" className="form-control" id="password" onChange={(ev) => {this.setState({password: ev.target.value})}} />
            </div>
            <button type="submit" className="btn btn-primary" onClick={this.save} >Aceptar</button>
            <a  href='/login' className="btn btn-primary">Cancelar</a>
          </Col>
        </Row>
      </div>
    )
  }
}