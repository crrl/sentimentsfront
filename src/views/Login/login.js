import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import CustomToast from '../../components/General/Toast/Toast';
import Axios from '../../axiosConfig';

export default class Login extends Component {

  state = {
    email:'',
    password:'',
    showToast: false,
    key:0,
    redirect: false
  }

  login = () => {
    let key = this.state.key+1;
    let payload = {
      email: this.state.email,
      password: this.state.password
    };

    Axios.post('/api/authentication',{payload})
    .then(res => {
      let expirationDate = (new Date().getTime() / 1000) + 3600;
      let token = res.data.token;
      Axios.defaults.headers.common['Authorization'] = res.data.token;

      sessionStorage.setItem('token', token);
      sessionStorage.setItem('exp', expirationDate);
      this.setState({
        redirect: true
      });
    })
    .catch(err => {
      this.setState({
        showToast: true,
        key: key
      });
    });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to='/'/>;
    }

    return (
      <div>
        {this.state.showToast ? <CustomToast show={true} key={this.state.key} message="Algo ha salido mal, intenta de nuevo!"/>: <span></span>}
        <Row className="justify-content-md-center absolute-center center" >
          <Col lg="3">
            <div className="form-group left-align white">
            <label htmlFor="email">Correo:</label>
              <input type="email" className="form-control" id="email" onChange={(ev) => {this.setState({email: ev.target.value})}} />
              <label htmlFor="password">Contraseña:</label>
              <input type="password" className="form-control" id="password" onChange={(ev) => {this.setState({password: ev.target.value})}} />
            </div>
            <div>
              <button type="submit" className="btn btn-primary" onClick={this.login} >Entrar</button>
              <a  href='/nuevo-usuario' className="btn btn-primary">Registrar</a>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}